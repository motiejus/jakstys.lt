{
  nixConfig = {
    trusted-substituters = "https://cache.nixos.org/";
    trusted-public-keys = "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY=";
    extra-experimental-features = "nix-command flakes";
  };
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";

  outputs =
    { self, nixpkgs }:
    let
      mkShell =
        pkgs:
        pkgs.mkShellNoCC {
          LOCALE_ARCHIVE = "${pkgs.glibcLocales}/lib/locale/locale-archive";
          packages = with pkgs; [
            hugo
            brotli
            zopfli
            parallel
            dart-sass
          ];
        };
    in
    {
      devShells.x86_64-linux.default = mkShell (import nixpkgs { system = "x86_64-linux"; });
      devShells.aarch64-linux.default = mkShell (import nixpkgs { system = "aarch64-linux"; });
    };
}
