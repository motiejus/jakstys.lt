# Simple Style Hugo Theme

My simple style hugo theme, based on [this webpage template](https://yanlinlin82.github.io/webpage-templates/simple-style/index.html)

Online demo of this theme: [simple-style-demo](https://yanlinlin82.github.io/simple-style-demo/)

## Supported Parameters

In `config.toml`

```
[params]
subtitle = "Sub title of the site"
favicon = "/<path-to-site-icon>/logo.ico"
githubUrl = "https://github.com/<your-name>/<site-proj-name>/"
referrer = "always"
author = "..."
description = "..."
keywords = "..."
googleSiteVerification = "<google-site-verification-code>"
search = "..."  # baidu, google, bing, duckduckgo
dateFormat = "Mon Jan 2 15:04:05 MST 2006"  # see: https://gohugo.io/functions/format/#gos-layout-string
externalLinkIcon = true
externalLinkNewWindow = true
```
