---
title: "Thank You Drew DeVault"
date: 2024-03-23T14:50:33+02:00
slug: thank-you-drew-devault
---

This is a long-overdue public thank-you letter to [Drew
DeVault](https://drewdevault.com/).

He has been an ethical FOSS steward for as long as I remember. There is a
number of examples where he stands for what's right and upholds his values.
Sometimes, perhaps unfortunately, pretty damn often, [getting shit for it][1].
Negativity on the internet doesn't seem to stop him: it obviously makes him
feel worse, but he persists and keeps doing what is right.

The most recent example, which pushed me over the line to write this post, is
[Redict announcement][2]. Quote:

> The source code is hosted on Codeberg, a Forgejo instance operated by a
> German non-profit, which should provide a comfortable and familiar user
> experience for anyone comfortable with the GitHub-based community of Redis®
> OSS.

This paragraph amazed me. Drew created and runs [SourceHut][3], a Codeberg
competitor. As much as it was tempting to use it he used a competitor's forge
to host the redict, while being the primary driver (and the prime decision
maker) for the fork.

Keep going, Drew, and thank you.

[1]: https://drewdevault.com/2022/05/30/bleh.html
[2]: https://redict.io/posts/2024-03-22-redict-is-an-independent-fork/
[3]: https://sr.ht/
