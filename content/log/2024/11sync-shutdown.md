---
title: "11sync shutdown"
date: 2024-07-01T00:04:30+03:00
slug: 11sync-shutdown
---

As you may have [read]({{< ref "log/2024/11sync-signup" >}}), I have created a
"hosted syncthing" pre-registration page in [11sync.net][11sync]. The
1-sentence pitch for the service is:

> 11sync.net is a Hosted Syncthing peer. Designed for Syncthing users seeking
> reliable backups and resilience.

Over the ~6 months sign-up page was live, 46 people[^1] signed up. This
sign-up activity signals this will unlikely be a sustainable business, so I am
halting all activity and will terminate the sign-up page soon. Thank you for
your interest if you have signed up, I appreciate that!

If you are interested, find a brief retrospective below.

Retrospective: timeline
-----------------------

* *2021:* start thinking about this idea, pitch to whomever will listen.
* *2022:* put together a PoC of the service: I would be comfortable offering it
  to non-malicious technical friends.
* *2022:* following Avery Pennarun's [suggestion][chasm-blog], read Geoffrey A.
  Moore's [Crossing the Chasm][chasm]. Stop all development, but occasionally
  bug my family, signalling the idea is still there.
* *2024:* my precious wife gets tired of me bringing the start-up idea up,
  urges to make a real move.
  * Come up with a name, purchase a domain, create a simple pitch.
  * Create a registration page in about a week (it' been lots of fun, since I
    haven't touched Django in about a decade; it felt like meeting an old
    friend).
  * Start donating $128/month and get an [11sync.net logo in syncthing
    page][archive]. I *guess* this is where my sign-ups come from, as I have
    done no other reach-out.
* *Now:* I am shutting down the registration page, [11sync.net][11sync], until
  expires, will redirect here.

All in all, the sign-up page, logo, reach-out to syncthing maintainers was a
very valuable exercise. I *almost* avoided the trap of creating a service that
barely anybody wants! If you have comments or strong feelings about this post,
you are, as always, welcome to [contact me]({{< ref "contact" >}}).

Some sign-up analysis
---------------------

Sign-ups by country:

```
+-----------------+-------+
|     country     | count |
+-----------------+-------+
| United States   | 12    |
| Germany         | 10    |
| Russia          | 3     |
| Lithuania       | 2     |
| Canada          | 2     |
| Bulgaria        | 2     |
| The Netherlands | 1     |
| Sweden          | 1     |
| Spain           | 1     |
| New Zealand     | 1     |
| Mexico          | 1     |
| Malaysia        | 1     |
| Japan           | 1     |
| Italy           | 1     |
| Ireland         | 1     |
| Hungary         | 1     |
| France          | 1     |
| Denmark         | 1     |
| Colombia        | 1     |
| Brazil          | 1     |
| Austria         | 1     |
| Argentina       | 1     |
+-----------------+-------+
```

By continent:

```
+---------------+-------+
|   continent   | count |
+---------------+-------+
| Europe        | 26    |
| North America | 15    |
| South America | 3     |
| Asia          | 2     |
| Oceania       | 1     |
+---------------+-------+
```

Those are consistent with [syncthing stats][stats], no surprises here.

[11sync]: https://web.archive.org/web/20240525154202/https://11sync.net/
[stats]: https://data.syncthing.net/
[archive]: https://web.archive.org/web/20240626093759/https://syncthing.net/downloads/
[chasm]: https://en.wikipedia.org/wiki/Crossing_the_Chasm
[chasm-blog]: https://apenwarr.ca/log/?m=201807

[^1]: they add up to 48. Two of them are mine.
