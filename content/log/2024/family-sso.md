---
title: "Family Single Sign On Was a Bad Idea"
date: 2024-11-23T21:41:39+02:00
---

When my eldest son became of an email-qualified age, I set him up a
[Bitwarden][1] instance, so he can keep his password there. He *loved it*,
because, it turns out, a 7-year-old finds a personal digital space important or
exciting or both.

My password manager instance is not open to the public internet, necessitating
use of [VPN software][2], which needed a method to authenticate (a.k.a. login
method).

As someone inundated into "best practice" security policies of a 10k+-engineer
corporation for quite a while, I built my home authentication system using the
only method I really respected: single-sign-on. I thought that having a single
password for all of our services will be convenient and quite easy. So I set up
SSO for a few services that my family uses, including the VPN software.

After thinking about it for ~2 years I realized that single-sign-on allows
companies quickly onboard new employees, as well as quickly revoke access from
leaving individuals. Expanding family is limited by obvious constraints, and
people generally leave families *very rarely*, making the main SSO selling
point quite moot.

To sum up, having a "single password" was unnecessary, since everyone use a
password manager anyway. In a small-family setting, where nobody generally
leaves "the company" and my time to maintain the SSO sand-castle is limited, it
is an unnecessary overkill. I am quite surprised it took so long to understand
that. Maybe my tolerance for self-induced bullshit is too high? Something to
consider.

So my next project is to de-tangle the SSO mess that I put myself in and create
separate users and app-passwords for each thing we use. The prospect of
executing this "migration" is not fun, but the end result will be better,
because things will get simpler for everyone.

[1]: https://github.com/dani-garcia/vaultwarden
[2]: https://headscale.net/
