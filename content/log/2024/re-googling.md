---
title: "I have successfully re-googled myself"
date: 2024-10-09T20:59:43-04:00
---

It is quite common across tech folk (at least the ones that I speak of) to
"de-google". I.e. get rid of Google (or, in general, big-corp) services. As you
can imagine, after running, say, e-mail for 15+ years, the process (and the
result) isn't fun.

I got rid of my Gmail account in the Christmas of 2023, so about 10 months ago.
The result was rewarding, but only because I felt "de-googled". It took just a
few weeks to realize that it is not the end of it.

# Reasons

I moved from Google because:

- Google Docs lost a couple of important documents. I did not take backups,
  because I trusted it too much. Logic: if it can lose a few documents, it can
  lose everything.
- Privacy: I did not want to be scanned by "default" on every step of my
  e-life.
- Possible account shutdown: an automated system may shut down my personal
  account due to a false-positive, and I will have no recourse. In that case, I
  would lose both my e-mail address (`desired.mta@gmail.com` at the time) and
  all the e-mails from the last 19 years.

Due to all of the above, I decided to pull myself out of Google's environment.
I have:

- Moved all my email to [migradu](https://migadu.com). This went relatively
  well and easily: control plane is good, the open-source/free e-mail clients
  are reasonable. I settled with
  [evolution](https://help.gnome.org/users/evolution/stable/) and [k-9
  mail](https://k9mail.app/) for the phone.
- Moved all the personal photos & videos to a directory in my file system. I
  lost all the things that I expected to lose:
  - indexing by location.
  - face recognition.
  - any preview of photos that are 3+ months old on my phone. I was not
    generally previewing old pictures on my phone, so it turned out OK.

    I installed [photoprism](https://www.photoprism.app/) to a computer in my
    closet, which allowed grouping by faces: face recognition was rather poor,
    but better than nothing, and I was able to see thumbnails.
- Google Docs: I downloaded all the interesting files as `*.xlsx` and `*.docx`
  and put them to the file system. This was not a big change, because after
  Google lost a few of the old documents, I was already suspicious and careful.
- Calendar. Oh, calendar. It brought warm and fuzzy memories and it was
  much, much harder to lose or replace than anything else.

# The Calendar

See, calendar is the problem and the thing I missed most dearly. For the last
15 years I was used to Google auto-creating calendar events from the e-mail I
had received. With migadu's IMAP (for email) and CalDav (for calendar), there
was no integration between the two, and it is painful.

Because calendar and e-mail applications are decoupled on the phone, if I add
an event attendee, they will not receive an invitation. If you think about it,
it is intuitive: why or how would [DAVx⁵](https://www.davx5.com/) e-mail to the
meeting attendees? Of course it doesn't, because it's not an e-mail client,
it's a synchronization tool.

Calendar frustration got me back, for the better.

# Status Quo

I moved all my e-mail (again) from migadu.com to Google, under my personal
domain. I use docs, e-mail and calendar now, all under `jakstys.lt`. I have
decommissioned my e-mail address of teenage years (`desired.mta@gmail.com`) and
haven't opened it in months.

Pictures are self-hosted using [immich](https://immich.app/). It provides
location indexing, facial recognition and a full-fledged mobile app. Can't ask
for more.

Having a calendar again is awesome.

I did not figure out the e-mail backups yet, but I am hopeful I will get to
them by the end of the year, when my migadu.com subscription ends.
