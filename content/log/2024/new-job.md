---
title: "New Job"
date: 2024-03-23T15:42:10+02:00
---

At 2024-03-12 I started at [chronosphere.io][1], the M3DB team. The team is
focusing on efficiency of [M3DB storage engine][2], and it so happens that the
team I joined are performance nerds. I have never seen so many flamegraphs and
profiler visualizations during a such short time span, which, given the team's
position in the company, shouldn't come as a surprise.

To date, I am the first Linux (desktop) user in EMEA. So far I made 2 pull
requests, which made Monorepo buildable and runnable on my [environment]({{<
ref "log/2023/nixos-subjective"
>}}). I will pick up the first "real" task next Monday.

[1]: https://chronosphere.io/
[2]: https://m3db.io/
