---
title: "bcachefs"
date: 2024-04-04T10:32:22+03:00
draft: True
---

Once bcachefs got [merged][1] to Linux 6.7, I was excited to try it out. Who
wouldn't? It intrigues me for a combination of features not readily available
on other file systems:

- *Tiered storage*: I want generally "fast" storage and "lots" of it (by my
  standards). Ideally, I would have a HDD with "cold" storage and SSD with
  "warm" storage (including writeback for fast writes). Currently all
  homeserver files are in a 2TB SSD drive. I would like to get an HDD and have
  a smaller (cheaper) SSD.
- *Compression*: compression ratio for homeserver files is `1.07x`, but over 2x
  for `/nix`. Compression ratio for `/home` on my personal laptop is `1.23x`.

Part 1: 

```
$ bcachefs fsck /dev/mapper/luksroot 
< over 1 million lines like this ... >
error validating btree node at btree rebalance_work level 0/1
  u64s 11 type btree_ptr_v2 53920:1262:U32_MAX len 0 ver 0: seq 914ce76a78ae3f22 written 281 min_key 0:19871775:1 durability: 1 ptr: 0:16177:0 gen 10
  node offset 141 bset u64s 94: invalid bkey: snapshot == 0
  u64s 5 type deleted 0:21863357:0 len 0 ver 0, fixing
error validating btree node at btree rebalance_work level 0/1
  u64s 11 type btree_ptr_v2 53920:1262:U32_MAX len 0 ver 0: seq 914ce76a78ae3f22 written 281 min_key 0:19871775:1 durability: 1 ptr: 0:16177:0 gen 10
  node offset 141 bset u64s 92: invalid bkey: snapshot == 0
  u64s 5 type deleted 0:21863362:0 len 0 ver 0, fixing
btree_node_read_work: rewriting btree node at btree=rebalance_work level=0 53920:1262:U32_MAX due to error
 done
journal_replay... done
check_alloc_info... done
check_lrus... done
check_btree_backpointers... done
check_backpointers_to_extents... done
check_extents_to_backpointers... done
check_alloc_to_lru_refs... done
check_snapshot_trees... done
*** buffer overflow detected ***: terminated
Aborted
```


[1]: https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=9e87705289667a6c5185c619ea32f3d39314eb1b
