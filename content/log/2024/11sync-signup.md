---
title: "11sync.net Signup"
date: 2024-03-23T15:20:47+02:00
slug: 11sync-signup
---

As some of you may have already noticed, I have launched a pre-registration
page for [11sync.net](https://11sync.net). Here is it's headline:

> 11sync.net is a Hosted Syncthing peer. Designed for Syncthing users seeking
> reliable backups and resilience. 

I came up with the idea after running syncthing semi-manually for a few years
now. At certain times I want to have a bootstrapped start-up of my own, and
this feels like something I want to and would be skilled to do.

I have launched the registration page in 2024-01-17, which, as of writing, is a
bit over 2 months ago. My marketing has been:

1. Become a sponsor of syncthing (e.g. scroll down in the [Syncthing
   downloads][1] page to find the logo).
2. Announced it in the [syncthing forum][2].
3. Announced it in [#11sync on Libera Chat][3].

26 people entered their e-mail address, 3 of which seem to be people I know.
The rate of sign-ups seems to be quite steady. Assuming a ~€10/month service
fee and an overly-optimistic whopping 10% conversion rate, that brings me to 2
users or €20/month.

I will keep the preregistration page open (and keep sponsoring syncthing), but
the market signal has been quite clear: there aren't hoards of people rushing
to sign up. :)

[1]: https://syncthing.net/downloads/
[2]: https://forum.syncthing.net/t/pre-announcing-11sync-net-a-hosted-syncthing-with-backups/21584
[3]: https://web.libera.chat/#11sync
