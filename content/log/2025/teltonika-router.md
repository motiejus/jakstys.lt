---
title: "Teltonika Router"
date: 2025-01-18T22:42:23+02:00
draft: true
---

This is my first product review. I really wanted to write it, because:

- from my very limited use, it promised a really good start.
- I haven't heard of the product line, even though the company HQ is a few
  kilometers from me (and is a well-known brand and employer, but not for
  routers!).

Background
----------

I am building a house. Huge thanks to my very helpful neighbors for putting a
webcam on his windowsill and sharing the feed (and recordings), I was able to
see a view the construction. It was *extremely* helpful for reasons I will not
get into here.

The construction will soon continue, and, thanks to the success of the
neighbor's camera so far, I decided to set up my own for a better, closer-up
view. I spent a good amount of time over the last few days looking for a device
combination that would:

- Do the feed. Friends of friends at [jpg.lt][jpg] recommended going with
  Dahua. The recommendation is consistent with [Frigate][frigate] [hardware
  recommendations][frigate-hw] page. I bought the camera from jpg.lt.
- 4G/5G modem, so the feed can be downloaded by the NVR. My first choice is
  Frigate, we'll see how it goes.
- WiFi router, so I have more on-site troubleshooting options, besides climbing
  a 4m pole to attach the RJ45 cable.
- Should be installable and service-able by a non-specialized contractor.
- Highly desired, but optional: tailscale support.
- Highly desired: as little devices as possible.
- All equipment will be outside, on a pole, in [Vilnius climate][vilnius],
  during winter & summer seasons.

Having been Mikrotik user for the last few years, that was obviously my first
choice. However, Mikrotik does not have a device that is both an LTE modem and
a WiFi router. Internet searches for this combination uncovered Teltonika
Networks. Teltonika, as you may know, is headquartered in Lithuania. I did not
know they are making routers right until this search. Bad marketing?

Anyhow, [RUTX11][rutx11] is the cheapest 4G+WiFi router that meets my spec (not
that it's cheap; they have more expensive stuff that's higher-specced, which is
just unnecessary for me). They also sell a [separate enclosure][enclosure] that
makes it into an outdoor-capable device (IP67).

Setting it up felt like using a yet-another consumer router, which is good.
This is a prerequisite, so I can recommend it to my non-geek friends. Setting
up tailscale felt like setting it up on a consumer router, glitch-free, which
is great.

OpenWRT origins
---------------

RutOS is based on OpenWRT, which is great, as OpenWRT is a solid choice
engineering-wise. Teltonika is a hardware company, not a Router OS company.
Talking from my personal experience, customizing OpenWRT does not take much
Router OS is not something where one needs to "differentiate". The
differentiation is in:
- the hardware build, which is very solid,
- hardware reliability and service-ability, about which feel free to ask me in
  2-3 years.
- software and hardware integration (the "add-ons").
- system upgrades.

[jpg]: https://jpg.lt/
[frigate-hw]: https://docs.frigate.video/frigate/hardware
[frigate]: https://frigate.video
[vilnius]: https://en.wikipedia.org/wiki/Vilnius#Climate
[rutx11]: https://teltonika-networks.com/products/routers/rutx11
[enclosure]: https://teltonika-networks.com/products/accessories/antenna-options/outdoor-ltewi-figpsbluetooth-antenna-for-rutx11-and-rutm11-routers
