---
title: "Construction site surveillance"
date: 2025-02-24T22:01:14+02:00
draft: true
---

I am building a house, for which I decided I need a webcam. I have never set up
a security camera, so have zero knowledge before starting; one good thing — I
knew the requirements pretty well:

- 24/7 on-demand live view, plus some recording: 7 days 24/7, plus some time
  for when and around "motion" is detected.
- Nothing that has a subscription fee. ring.com is pretty good for short-term
  plug&play, but I cannot brace myself for a yearly payment, especially that I
  have (and can afford to maintain and upgrade) storage, network and compute.
  Network subscription is a fine service to pay for, SaaS is not.
- Use off-the-shelf hardware, so I am only minimally required to maintain the
  setup (so no DYI webcams or routers).

# The Setup

Since the building site is "remote" (there is no existing infrastructure
besides electricity), networking needs to be self-contained. This is the setup:

```
           o-
          /\
        Camera
          |
    LTE/5G Router
          |
   <public internet>
          |
         NVR

```

* Camera is the single most important component. Easy to understand what it is
  and why it matters.
* One can easily "live view" directly in the camera stream. Cameras can usually
  record video into a builtin SD card. However, that's not very useful if it
  gets vandalized or stolen, so better push that video somewhere safe.
* *NVR* is a Network Video Recorder. Besides the camera, this is the *second
  most important component*. NVR captures a video stream from the camera,
  (optionally) detects people and vehicles, and records everything. Since NVR
  is the main "interface" to the surveillance system, not the camera, it is
  important NVR has a good UX. You don't care about the camera UX after it's
  set up.

# Picking the camera

[A friend of a friend](https://jpg.lt/), who has been setting up security
cameras for the last 15 years, recommended Dahua. I picked a model and went
into it.

There are a few variables you may want to check:
- Pan, Tilt, Zoom (*PTZ*). Some cameras can change the viewing position
  remotely. I picked one with PTZ, but more out of curiosity than necessity.
  Turns out, ONVIF (the "open" protocol to control PTZ cameras) is very poorly
  supported with the NVRs I've tried.
- Resolution versus visibility in low light. [ipcamtalk.com][2] has decent
  recommendations, start there.
- Do your research in the [website][2], there are some great tips. I wish I had
  known about it, or at least read the Dahua part, before purchasing mine.

Note that being surveillance companies, they unsurprisingly have [bad
reputation in the Chinese-controlled areas][1]. I also found out about it only
while researching open-source NVRs.

# Network Video Recorder

Once you've settled on the camera (and the number of cameras), there are mostly
two NVR options:
- A dedicated set-top-box-sized device from the camera manufacturer. These are
  completely hands-off. A hard drive is usually purchased separately, depending
  on how much should be recorded. The UX experience is "it is what it is". I.e.
  camera manufacturers may or may not be the best NVR UX designers, especially
  when it comes to viewing the recordings or live stream remotely.
- Open source NVRs, which you can install to your existing home server. I
  considered [frigate][3], [Moonfire NVR][4] and [ZoneMinder][5]. Since I use a
  [home server]({{< ref "log/2023/nixos-subjective" >}}), I am self-hosting my
  NVR.

*Moonfire NVR* seems like the simplest of the bunch: low hardware requirements
(raspberry pi 2!), minimal number of features. Cons: does not have object
detection, not even "in theory". I have relatively powerful hardware in the
closet and want object detection for it.

*Frigate* seems to be what the kids use these days. Documentation is extensive,
but sometimes not very accurate. It took a few evenings to get it to work, and
it works.

*ZoneMinder* is the oldest and most established. I learned about it only after
having set up Frigate.

## Setting up Frigate

A few tips before you get started:

- for object detection you will need hardware support. Look at [recommended
  hardware][6]. I went with Google Coral, but I've heard good things about
  OpenVINO too.
- start with go2rtc from the get-go. I had to re-configure all the Frigate
  streams, which was way more annoying than it's worth if you start with
  go2rtc.
- as of writing I have [a performance problem with detectors using lots of
  CPU][7] that nobody seem to be able to reproduce. I am hopeful Frigate 0.15
  will fix this.

## Connecting the camera

I bought [Teltonika Rutx11][8] 4G router/wifi modem that will fit in the
[camera junction box][9]. Antennas will be outside:
- [External 4G antenna from Mikrotik][10]
- [Wi-fi dual-band magnetic sma antenna from Teltonika][11].

I purchased an unlimited 4G plan from an ISP that has good connectivity in the
area. Then connected the Rutx11 via tailscale/[headscale][12]. Using tailscale
I can connect to the camera directly from both my NVR and all personal devices,
a yet another tailscale+headscale recommendation.

## Bandwidth and codec considerations

I am pushing two camera streams, encoded in h.265 (resolutions TBD) using a
constant 4.1Mb/s. Both streams are transcoded to h.264 via go2rtc and then sent
over to Frigate and live view.

Using exactly the same parameters (resolution, fps), but with h.264, the
bandwidth grows to 11 Mb/s. Which may not sound like a lot with today's fiber
everywhere, but is considerable on a 4G connection.

Since the home server has a graphics card, it can use hardware acceleration for
video encoding and decoding. Thanks to proliferation of online video platforms,

It takes ~5-10% of a single CPU core to transcode a stream, depending on the
resolution. In my case, I am transcoding 4 streams (2 cameras, "low" and "high"
res each), so in total transcoding uses about half of a core with minimal
bandwidth.

Once the house is complete and I move NVR to the same physical network, I will
change the encoding to h.264, stop transcoding and use more video bandwidth
locally.

[1]: https://uhrp.org/statement/hikvision-and-dahua-facilitating-genocidal-crimes-in-east-turkistan/
[2]: https://web.archive.org/web/20250221205936/https://ipcamtalk.com/wiki/ip-cam-talk-cliff-notes/
[3]: https://frigate.video
[4]: https://github.com/scottlamb/moonfire-nvr
[5]: https://zoneminder.com/
[6]: https://docs.frigate.video/frigate/hardware
[7]: https://github.com/NixOS/nixpkgs/issues/381280
[8]: https://teltonika-networks.com/products/routers/rutx11
[9]: https://www.dahuasecurity.com/products/All-Products/Accessories/Camera-Accessories/Camera-Mounts/Junction-Boxes/PFA126
[10]: https://mikrotik.com/product/mant_lte_5o
[11]: https://teltonika-networks.com/products/accessories/antenna-options/wi-fi-dual-band-magnetic-sma-antenna
[12]: https://headscale.net/stable/
