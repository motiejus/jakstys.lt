---
title: "My Favorite Podcast"
date: 2022-11-30T23:40:27+02:00
slug: my-favorite-podcast
description: "A short plug about my favorite podcast."
---

The only podcast I was able to listen for more than 3 series was [BBC][bbc]:
mostly "history" and "life stories". Over the last ~10 years I tried many
others, but nothing clicked for a longer engagement.

A few months ago I stumbled across Adam Gordon Bell's
[corecursive][corecursive]. My first episode was with [Richard Hipp][hipp], the
creator of SQLite. It was great. The second one was with about [zig][zig]. The
average episode is so good that I just downloaded them all to slowly go through
them.

If you want to take out something from this post, listen to the three episodes
below, even if you are not a developer yourself[^1]:

- [The Untold Story of SQLite With Richard Hipp][hipp].
- [Full-Time Open Source With Andrew Kelley][zig].
- [Memento Mori With Kate Gregory][kate-gregory].

The last in this list triggered to write out this recommendation. I spent a few
weeks trying to put my finger why I like Adam's podcast so much. And today I
understood: Adam is empathetic, he can listen, and really, really make people
talk.

I recommend listening to the three above, even if you are not a developer
yourself. You may find yourself in my position — downloading the archive and
becoming inspired.

[^1]: in that case I recommend starting with Kate Gregory.

[bbc]: https://www.bbc.co.uk/sounds/podcasts
[corecursive]: https://corecursive.com/
[hipp]: https://corecursive.com/066-sqlite-with-richard-hipp/
[zig]: https://corecursive.com/067-zig-with-andrew-kelley/
[kate-gregory]: https://corecursive.com/memento-mori-with-kate-gregory/
