---
title: "Side project retrospective"
date: 2022-08-21T06:15:56+03:00
description: "I worked on a side project from 2022 Jan until 2022 August. This is a short retrospective."
---

I spent 7 months building a project (hosted [here][turbonss]). Goal: make a
fastest known user/group NSS database. I succeeded, kinda. Here is an excerpt
from a conclusion I wrote this morning:

    This is the fastest known NSS passwd/group implementation for *reads*. On a
    corpus with 10k users, 10k groups and 500 average members per group, `id`
    takes 17 seconds with the glibc default implementation, 10-17 milliseconds
    with a pre-cached `nscd`, ~8 milliseconds with `turbonss`.

Guess what? It is fastest, but not by enough (compared to nscd) to matter.

In lines of code:

    ===========================================================================
     Language        Files        Lines         Code     Comments       Blanks
    ===========================================================================
     Markdown            3          522            0          422          100
     Zig                23         5560         4601          242          717
    ===========================================================================
     Total              26         6082         4601          664          817
    ===========================================================================


This enterprise reminds me of a project I did back at Amazon: I set out to
write a faster and more memory efficient throttling daemon (an API-compatible
CoralThrottle replacement). It took me ~1 month to build something that can be
meaningfully compared. Then while comparing the two I discovered that
CoralThrottle was not configured correctly on our environment. When I fixed
that, my implementation was even slower (to be expected!) than the upstream
one.

Story repeats itself.

How was it?
-----------

Loads of fun! I learned Zig quite a bit, [contributed][zig-pr-motiejus] to the
stdlib. I can comfortably say that it is an amazing language for systems
programming. The best parts are C interop and comptime.

The initial motivation cooled down after 1-2 months, then I needed discipline
to get back to it. After 5 months of work I did not even start writing the
system integration part, so for a very long time I had nothing but unit tests
to see the progress.

I did not let myself start another project until this is finished. That was my
way to keep the "motivation" up. Now, on the morning of Sunday, 2022-08-21, it
is finished.

What's next?
------------

I will weather-proof the furniture in my terrace. :)

On a serious note, I don't know what I will do with computers "on the side".
Maybe something with maps, finally. You will find it here.

[turbonss]: https://git.jakstys.lt/motiejus/turbonss/
[zig-pr-motiejus]: https://github.com/ziglang/zig/pulls?q=is%3Apr+author%3Amotiejus+
