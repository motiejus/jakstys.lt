---
title: "In Defense Of Big Tech Hiring"
date: 2022-06-23T13:45:00+03:00
slug: big-tech-hiring
description: "BigTech hiring is pretty much the same across BigTech. I think it's more good than bad. This post explains why."
---

There is quite a lot of negative sentiment about broken BigTech hiring
processes. If you have not heard, these are good introductory posts:
- [Dan Luu — Misidentifying Talent (2022)][danluu-talent].
- [Dan Luu — Hiring Lemons (2016)][danluu-hiring-lemons].
- [Dan Luu — We only hire the trendiest (2016)][danluu-trendiest].
- [Thomas Ptacek — The Hiring Post (2015)][tptacek-hiring-post].

The gist of the posts above is:
- BigTech hiring process is pretty much the same across BigTech. It is
  explained in this post, even, in detail. Therefore, the companies are
  applying the same criteria, and thus fighting for the same candidates: the
  candidates that do well in these particular interviews, but not necessarily
  at the job.
- Because the filter is the same, it does not encourage diversity in candidate
  background.
- The interview process, and especially the problems, are not representative to
  what the dayjob will require. Some links above offer alternative methods of
  interviewing.

Why am I writing about this? I will be conducting an [Uber Mock
Interview][uber-mock-interview] later this month. I meant to write about
seemingly broken BigTech interviews for a while now, but this event offered me
a concrete deadline. I feel like I have to explain to myself why I am doing an
interview at all, and why I am OK with the format we are planning to use. In
this post I will:
- Walk through the "standard" interview process of a Big Tech. [I've worked at
  two]({{< ref "resume" >}} "Resume Page"), the process is very similar; will
  shamelessly extrapolate for "most others". (This matches the experience of my
  friends whom I have surveyed.)
- Explain some reasons why *I think* the interviews are done the way they are,
  and why we reasonably expect them to work, despite the negatives.
- Talk about the limitations/caveats of the process, along with mitigations how
  we can work around them.

Usual disclaimer: this is my personal opinion and this blog is not affiliated
with my employer in any way.

Jump to the bottom for the [conclusion](#tldr-so-does-it-work).

## Trade-offs of the standard interview process

This is how a standard[^1] interview loop in the big techs I've worked so far
at looked/looks like:

1. Recruiter: CV screen + chat over the phone: 30m-60m.
2. Engineer A: Phone screen: 1h.
3. "Business loop": 5 interviews in a single day, 1h each:
   - Engineer B: Coding 1.
   - Engineer C: Coding 2.
   - Engineer D: Design & Architecture.
   - Manager: Hiring Manager.
   - Manager[^2] or Engineer E: Bar Raiser.
4. All participants above: Debrief, where hire/no-hire decision is made:
30-60m++.

Such process somewhat works for the BigTech and individuals. The primary goals
of the BigTech seems to be:
- Apply a consistent filter and make sure candidates are evaluated fairly.
- Get the best candidates for the environment.
- Fit into the constraints. E.g. it makes sense to invest only X amount of
  hours to interview each candidate, given their acceptance rate is Y%.

We, as engineers, have our goals. Usually they are:
- BigTech benefits. (Will not enumerate them here, but I will buy you a coffee
  and tell you in person if you want to know more.)
- Long interview process: it takes time and energy. Lots of energy. Like
  everything that takes time, energy and is stressful, we want to get it over
  with quickly.

I will be focusing mostly on the "consistent filter" and the "stress/energy"
part. About which Thomas Ptacek [states][tptacek-hiring-post]:

> The majority of people who can code can’t do it well in an interview.

Well, let's see how this holds. But first let's drill into the process.

### Recruiter: CV screen

CV screen is conducted by a recruiter in the HR department: I do not take part
in this, therefore I have no visibility into rejected candidates. To the
recruiters’ credit, judging from the resumes I've seen during phone screens, we
interview folks with diverse backgrounds, even with a minimal "match." For
example, a physicist major with data analysis background in Python is unusual,
but not very surprising: they do get a fair chance at the phone screen.

Some are truly unlucky: perhaps your experience does not match the recruiter's
understanding of what is necessary for the role. Or perhaps you want to change
direction and have spent a long time preparing for it, which does not show in a
resume. Or maybe you are like me and just don't know how to make a good resume.
Or all of the above. What then?

You can often circumvent the CV screen if you know someone at the company and
ask for a referral — which I openly encourage: if you want to work at Uber in
Vilnius, [ping me directly][ping-me]. We are able to submit referrals, which
skips the standard CV screening, because, ahem, I am doing the screening. I can
use my own criteria for evaluating your background and experience. I will note
your open-source contributions, especially pull requests, pull request reviews,
communication issues and, of course, code you've authored. Bonus points for
maintaining a project: changelogs, mailing list submissions, etc.

I have all the incentives to refer my friends and people I don't know (yet),
because:
- My referral is a pretty weak signal to the hiring committee, so I take
  minimal risk. I can take bets, I do, and some of them work out. Some of my
  referrals are rejections at the first technical interview, and that's OK.
- Some of my referrals turned out to be *excellent* people that I did not even
  appreciate beforehand, because I did not know them enough. (E.g. because we
  did not see each other since high school, not to mention any professional
  interactions. Hello, Ignai.)
- We get a monetary benefit: a referral bonus. The amount is relatively small,
  thus not worth the time investment alone. It is, however, comfortably enough
  to pledge: If you want to talk about possible transfer to my team/company,
  let's meet. I will buy you a coffee from the referral money. No strings
  attached.

To sum up: if you can't get past the CV screen phase, look for friends, ping
engineers. If you don't know anyone working in the target company, you may find
them online. If you show reasonable politeness and promise, we will be happy to
refer you, increasing your chances of success.

### Engineer A: phone screen

The first phone screen is usually the first candidate's interaction with an
engineer. The phone screen (these days via a video link) usually consists of:
- ~30-40 minutes: a "simple" coding challenge.
- ~20 minutes: interviewer selling the company and the position. We spend quite
  a bit of time explaining what we do and answering questions that candidate
  cares about. Work-life balance, how we do planning, how frequently we have to
  work during non-working hours (e.g. due to meetings with the US), what is the
  office like, equipment, etc.
- ~10 minutes, optional: very brief inquiry about concrete past experiences to
  determine candidate's "level" and "experience". Some interviewers do it. I
  don't do this part, as I prefer more coding time.

There are a couple of aspects worth highlighting:
- The interviewer (Engineer A) *alone* decides on go/no-go.
- The interview also has very little accountability to reject the candidate;
  later stages require them to explain their decision, whatever that is, in the
  debrief.
- The interviewer decides on the interview problem and the interview format,
  which leads to highly inconsistent phone screens across interviewers.

Therefore, I think this part is the *most subjective and punishing* in the
whole loop.

Unfortunately, many people fail at this stage. An engineer is put into a
position to understand, solve, code and debug a simple, but non-trivial problem
in 30-40 minutes. Such high-stress high-stakes situation barely happens in life
*except* at the interviews. I believe this phase is most susceptible to the
Thomas Ptacek's quote before. How can we help ourselves? Well, first let's
understand what the most frequent causes for failure are:
1. The candidate is not up to speed with their programming language's
   primitives that are often necessary during coding challenges. E.g. they may
   struggle an unreasonably long time to read file to an array, just because
   it's been years since they needed to simply open a file in their dayjob.
2. The candidate is visibly nervous and is making silly mistakes they would
   never do in a non-stress situation.
3. For some reason the candidate feels obliged to a language that Uber uses
   (e.g. Go) even if they are not comfortable in it. I always ask candidates to
   pick literally any language that they're very comfortable with. One of my
   colleagues did the Uber interview in Haskell, and Uber's Haskell footprint,
   being honest, is very very minimal (but nonzero).

How can bright candidates mitigate this? Practice. To bring up to speed with
the primitives, a couple dozen coding challenges in your favorite "coding
challenge site" will help. How can they mitigate the "stress" part? Also
practice, but with a friend. Once you have practiced the "coding challenge
site" alone enough, take a friend/spouse/anyone to pretend being the
interviewer. Do the same again. Drills with friends do not remove the stress,
but certainly help.

Is it ridiculous? Yes and no. Yes, because, to be accepted at a BigTech, you
need to practice for things you will not do at job. No, because, like Patrick
McKenzie points out, it's worth to spend a few weeks [learning something you
will never need at your job][salary-negotiation]. It makes sense to be good in
programming puzzles for exactly the same reasons it makes sense to learn how to
negotiate.

### Engineers B and C: Coding

The "business interview" usually starts with 2 coding interviews. Conceptually
they are similar to the phone screen, with less focus on "selling" and more on
"coding". However, these are safer to the candidate, because, if the candidate
excels the interview B, and fails the interview C, they still still have a
decent chance to be hired. Besides the safety net for the candidate, the rest
of the experience is similar to the Phone Screen: I truly believe that passing
this stage is a matter of practice.

You may ask me: why bother with a puzzle at all, since it is not representative
of what we do at work anyway? Because of two reasons:

1. We need a proof you are able to do things that are required for an engineer
   anyway:
   - Understand the task.
   - Come up with a solution: on your own or with guidance.
   - Explain the algorithm.
   - Code it.
   - Write tests for simple cases.
   - Write tests for edge cases.
   - Debug issues and find bugs.
   - Reason about the solution's efficiency.
2. We need a somewhat consistent way to calibrate across candidates. E.g. I
know that, if a candidate reaches point X of my exercise, they pass my phase of
the interview.

On reasoning about the solution's efficiency, people often think efficiency
boils down to `O(<...>)`, but it can be much more than that: venues for memory
leaks or garbage collection (depending on the language), non-memory-non-cpu
resources to achieve the task (e.g. network, file descriptors; you can go far
into the effects of memory pressure from the negative dentry cache even if you
started with a seemingly simple coding exercise).

I simply do not know of a better way to achieve the above in 3 hours or less.
We know coding puzzles are not perfect, because it requires candidates to
prepare for things they would not do otherwise. But motivated candidates do
prepare. And we have a way to calibrate them.

### Engineer D: Design & Architecture

An engineer will ask you to come up with a solution to a problem they may be
more familiar with than you. This hour is a proxy to understand if the
candidate:

- Is aware of software architecture as a concept: what is it and what is it
  for?
- Has built something themselves? Did/do they maintain it?
- Offering the trendy or a reasonable thing (these are often very different)?
  If they choose the trendy, why? Do they understand the drawbacks?
- Understands limitations of the system they have built "on the whiteboard"?
- Understands the trade-offs? What alternatives have they considered, ruled
  out, and why. This tells a lot about candidate's experience in a domain and
  their decision making process: such talk is less sensitive to "mood of the
  day" that can ruin the coding exercise, and provides a lot of signal.

Unlike the coding challenges, everything we test in Design & Architecture
interview is critical at work: we need to understand many systems, both ours
and of others', in a similar way you would do in the interview usually by
reading the documentation and surveying owners for knowledge gaps. Once the
limitations of your dependencies are understood, write code having them in
mind.

If you have never worked at a place to muscle your Design and Architecture
skills, do not worry: when you let us know this isn't a thing you've had a
chance to learn, we'll still try to work with you towards a solution based on
what you do know and we’ll use that to assess your ability to pick this up on
the job.

Unfortunately, I have seen candidates trying to dishonestly cheat (a.k.a.
"bullshit through"). It is easier to spot such cases than you may think. "How
exactly would you observe this behavior?", with varying degrees of "exactly",
is a good start to catch cheaters. Such behavior will definitely void your
application. Don't do it, be honest.

### Manager: Hiring Manager

I have never been in a hiring manager or a bar raiser interview (except as a
candidate a long time ago), so can only point out what I generally hear in the
debrief. If I am wrong, I apologize: please let me know.

Hiring Manager interview usually entails:
- Determine if the candidate has relevant skills for a particular role.
- Sell the position to the candidate.

Hiring Manager also assesses people and team skills together with the Bar
Raiser: see below.

### Manager or Engineer E: Bar Raiser

Bar Raiser will ask about your past experiences and determine your ability to
work in the team. "Tell me about a time" is a popular question prefix.
Bullshitting through this one is as hard as in the Design & Architecture
interview: you are dealing with a manager who is good at people-skills
(otherwise they wouldn't be a manager) and often had managed a team or teams
for a decade or more.

People are really good at spotting dishonesty. Your chances are much higher if
you are honest: admitting your weaknesses is better than trying to hide them.

If you are not a good team player, that will likely be determined during this
or the Hiring Manager's interview. That may be OK depending on the position;
but more often than not, this is a red flag.

## TLDR: so does it work?

Given the BigTech constraints, the interview process does what it's meant for.
It is not perfect: it sometimes leads to non-diverse candidates, folks trained
for the interviews but not the job, companies fighting for the same population.
However, it does fit the company constraints, and, in my experience, the result
is pretty damn good.

Does it work for us, though? Not always, because:
- Some may not be willing to invest that much time into job hunt. Well, that's
  on the candidate. Do it, it's worth it.
- The interview part, especially the full "on-site", is extremely stressful. It
  is what it is and that is unlikely to change any time soon. But it can be
  mitigated, as explained earlier.

If you are rejected or are too stressed for a BigTech interview, but still want
to work there:
- If you know someone at the company you want to apply to, ask for a referral.
  For example, you can [ask me]({{< ref "contact" >}} "Contact Page")[^3].
- Do some puzzles before the interviews. This is an investment that will pay
  off; just like spending some time to [learn to
  negotiate][salary-negotiation].
- If you fail, the recruiter usually tells why. Ask them when you can re-apply.
  If you didn't ask, the usual "wait time" is 6-12 months.

Hopefully you work, or will soon, in a job that suits you best. Regardless if
it's a BigTech or not, good luck!

# Addendum: a mock interview

Now let's talk business. I will be running an [Uber Mock
Interview][uber-mock-interview] on 2022 June 29th. The mock interview
familiarizes potential candidates with the interview process, hopefully
reducing uncertainty, and thus stress. Listening for a presentation about
Uber's interview process sounds boring; looking at a live interview — much less
so. For obvious reasons, we cannot live-stream a candidate, we will do the next
best thing: conduct an interview which is for all intents and purposes real,
except I will not get or lose a job if I make or fail it.

So we will be conducting a mock interview. Because it should be educational
*and* fun, this is what we will do:
1. I will not know the exercise beforehand. We will all see it, including
   myself, at the same moment.
2. I have not done a technical interview for >6 years now, so I did *a bit* of
   preparation. Not too much though, like most of us when applying for a job.
3. I may fail the interview, therefore I know I will be stressed more than at
   my work desk during regular coding. :) Stress is a very real interview
   experiences for everyone. So you get to see the "mock interview" on
   steroids.

We will not be able to publish the recording for legal reasons, so, if you are
curious, you have one shot to attend live.

P.S. The candidates can use any programming language during the interview. Make
a wild guess which I will pick.

Many thanks to Abhinav Gupta, Tim Miller, Anton Lavrik and Rick Boone for
reading drafts of this.

[danluu-talent]: https://danluu.com/talent/
[danluu-hiring-lemons]: https://danluu.com/hiring-lemons/
[danluu-trendiest]: https://danluu.com/programmer-moneyball/
[tptacek-hiring-post]: https://sockpuppet.org/blog/2015/03/06/the-hiring-post/
[ping-me]: https://news.ycombinator.com/item?id=31304657
[salary-negotiation]: https://www.kalzumeus.com/2012/01/23/salary-negotiation/
[uber-mock-interview]: https://www.meetup.com/uber-engineering-events-vilnius/events/286542203/

[^1]: for 90%+ of the Software Engineering roles. The rest 10% are interns or
  "super-senior" level engineers, hiring whom is above my pay grade.
[^2]: Manager, Director, VP, et cetera. The point is, People manager.
[^3]: If you want to work where I work (company + location), feel free to ask
  me for a referral. Keep in mind, though, that I will spend some time to
  understand whether I believe you are a good fit. See the post for my
  criteria. I will also buy you a coffee. Seriously; all you need to do is ask.
