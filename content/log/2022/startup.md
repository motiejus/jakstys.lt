---
title: "How I stopped my startup just before building it"
date: 2022-07-18T12:55:00+03:00
slug: startup
draft: True
---

I almost started building a startup and stopped on time. I saved 2 years of my
life! Here is the short version:

- I had the product idea, name, the landing page, a domain, a reasonable
  financial plan.

The landing page is [here](/2022/synctech.html), and here is a screenshot:

- I just needed to buy the domain name and start making the product.
- Something did not feel quite right, so I kept sitting in the "ready-set--..."
  phase for a couple of months.
- I read a book that [Avery Pennarun][apenwarr] recommended: [Crossing the
  Chasm][book] by Geoffrey A. Moore.
- I am not doing this startup and keep having fun [at]({{ ref
  "log/2022/uber-mock-interview-retrospective" >}}) [work]({{ ref
  "log/2022/how-uber-uses-zig" >}}).

What was the startup about?

[apenwarr]: https://apenwarr.ca/log/20180724
[book]: https://en.wikipedia.org/wiki/Crossing_the_Chasm
