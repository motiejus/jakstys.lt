---
title: "7 Years at Uber"
date: 2023-02-10T01:54:23+02:00
slug: 7-years-at-uber
description: "My last day at Uber will be (was, depending on when you read this) on 2023-06-30. This is a retrospective of the last 7+ years working at Uber's infrastructure."
---

This month turned 7 years since I joined Uber. Since it will be the last (a bit
more on it in the next paragraph), I though it's good to write a retrospective
on how it looked like.

On 2022-09-07 it was announced that Uber engineering site will be shut down on
2023-06-30. Whoever is willing to relocate are welcome to do so. The rest will
be laid off. The "easiest" offices for relocation are where our organization
is: Aarhus, Amsterdam, Seattle, New York, Sunnyvale and San Francisco. In my
case, I have a good life in Vilnius and want to keep it that way. So my days
are numbered.

Note that this started *before* the "layoffs of 2023" in the industry. Given
the conditions (9 months notice!), it does seem like a strategic move, not a
cost-saving one.

Chronological summary of my employment and projects at Uber:

- 2016-02-01: joined in Amsterdam, Marketplace Topology team.
- 2016-10-30: moved to Vilnius, Lithuania. Joined "Foundatoins Platform" team
  (probably it had a different name back then).
- 2017-2018: worked on an internal project in infrastructure. Not much to say
  about it, it is gone now.
- 2019-2021: worked on interesting and important infrastructure work: OS, host
  access, user+groups NSS module and distribution layer. An exploratory
  personal project with the same theme is [turbonss][turbonss].
- 2022-2023: bootstrapping arm64 for compute. [I wrote a bit about this]({{<
  ref "log/2022/how-uber-uses-zig" >}}). There were many past-2022-April
  developments, and I am working on an official Uber engineering blog about
  this. Should come out in a couple of weeks.

What was so noteworthy?
-----------------------

I met and worked with many amazing people. If I were to single out *one*, that
would be Dan Heller, who hired me in Amsterdam, and who I've been working on
and off ever since. Among other things (a book!), he wrote [10
tenets](https://medium.com/@daniel.heller/ten-principles-for-growth-69015e08c35b),
which I regularly use and recommend to others. I am not putting a second name
here, because where do I stop?

I used to do 2-3 yearly 5-12 day US trips. At the beginning almost exclusively
San Francisco. After ~5'th trip or so I started to look for things to do during
the weekends and (quite by accident!) found likeminded individuals who are
willing to join for crazy drives/flights/weekend trips with me. And oh the fun
we had together during those short stunts.

{{<img src="_/2023/7-years-new-mexico.jpg"
    alt="A selfie of two male individuals with two parked cars and a desert in the background."
    caption="One of the best weekend trips with Rick Boone (right) and Bob Nugman (not in the picture) in New Mexico just before a hike in the desert."
    class="half right"
    half="true"
    hint="photo"
    >}}

The engineering culture is very strong, I learned many valuable lessons: both
from anecdotes and actual experience of:
- Built something that nobody needs. I spent my sweat, blood and tears doing
  it. I had a small child at a time, and, unfortunately, sometimes my
  priorities were not chosen wisely. I am older now and hopefully wiser.
- Built something the organization really needs: an [NSS][nss] module with a
  distribution layer. I saw the steady uptick in use. The edge cases, scaling
  problems, lots of customer support (and documentation improvements). Lessons
  of ossification, client-side metrics, privy of my dependencies.
- Tried the corporate promotion lottery. 3 times! The third time was
  successful. I originally thought it was a lottery (when I "lost" it). Turns
  out it does not matter how much competence I show, or work I do, on a project
  that does not matter as much for the rest of the organization. I got my
  promotion for NSS and host access, things that others actually needed and
  valued.
- We did [Crane][crane], god damn it. A 5-year org-wide project that completely
  changed how we do infrastructure. Lots of technical and organizational
  complexities. We had very strong leaders, a clear vision and stamina to keep
  going. Crane was finished[^1]  on the week of this blog post.
- Onboarded a new novel technology (zig) and established a relationship between
  Uber and the Zig Software Foundation. Zig is currently on critical path at
  Uber, *probably* the second most-invoked compiler after Go. I am proud of
  that and hope it will serve as a beachhead for the conservatives. If the
  previous sentence does not make sense, look up ["crossing the chasm
  beachhead"][ddg]. Thanks to my weekend drive during the previous business
  trip (probably my last with Uber), I met Andrew Kelley in his home town
  Portland. We had lunch in the food court and went for an exactly 2-hour hike.
  Besides learning and onboarding new technology, I feel very much part of the
  community.

{{<img src="_/2023/7-years-portland.jpg"
    alt="Three people eating in a busy foot court outside."
    caption="A weekend in Portland from my last business trip. From the left: Andrew Kelley, Rick Boone and my manager Rolandas Tamašauskas."
    hint="photo"
    >}}

The end is near
---------------

There is much, much more to cover than the above, I had a great time. Great
people, experiences and organizational awareness. I even did 5 trips as a
driver during working hours, and this was encouraged by my manager and the
team. I am still having a good time, but, given the expiry date, it is
different: the office is shrinking, colleagues either leave the company or
relocate to Amsterdam or the US.

I will take the summer off. I may do some consulting in Autumn. So if you want
to talk build systems, systems programming or arm64, [contact me]({{< ref
"contact" >}}).

[^1]: The last host in the "legacy stack" was decommissioned.

[nss]: https://en.wikipedia.org/wiki/Name_Service_Switch
[ddg]: https://duckduckgo.com/?q=crossing+the+chasm+beachhead
[turbonss]: https://git.jakstys.lt/motiejus/turbonss
[crane]: https://www.uber.com/en-ES/blog/crane-ubers-next-gen-infrastructure-stack/
