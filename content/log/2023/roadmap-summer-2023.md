---
title: "Summer Roadmap 2023"
date: 2023-05-15T08:38:08+03:00
---

Now that I have 6 weeks of employment remaining, my wife encouraged me to come
up with something to do for the summer, and possibly after it. This post will
list a few ideas I've been thinking about.

Next 6 weeks of employment
--------------------------

The roadmap [described 6 weeks ago]({{< ref
"log/2023/my-zig-and-go-work-for-the-next-3-months" >}}) is still mostly
applicable. Quick update:

- The blog post I have mentioned [has been published][uber-zig-blog-post].
  Hooray.
- For the next 6 weeks I will work on making the Zig/Go ecosystem as best as I
  can. Many thanks to my leadership for understanding the long-term importance
  of this initiative.
- I have de-prioritized `hermetic_cc_toolchain`'s MacOS target support.

[uber-zig-blog-post]: https://www.uber.com/en-US/blog/bootstrapping-ubers-infrastructure-on-arm64-with-zig/

With employment out of the way, what is waiting for me past June 30?

Project 1: City Limits
----------------------

Hereby is my definition of "living in a city" for the purpose of this project:

> If there is a coffee shop within 15 minute walk from a location, then the
> location is indeed a town/city location. Locations outside a 15-minute walk
> are "not a city": suburbia, farmstead, homestead, whatever you want to call
> it.

Given the above definition of a city, I want to draw the a map of *Vilnius City
Boundaries*. I have not found the suitable algorithm yet: there are many
algorithms to calculate the duration from point A to B, but not an algorithm
which will draw the boundaries from point A (the coffee shop) to all reachable
points under a certain [cost][cost].

Of course, this definition is a joke. Not all of us live for coffee. I have a
few more applications for extending such a map, but the coffee is the first
one.

[cost]: http://theory.stanford.edu/~amitp/GameProgramming/MovementCosts.html

Project 2: my take on universal headers
---------------------------------------

Caution: this section is highly technical and probably interesting only to
folks who compile lots C or C++ applications.

Zig [universal-headers][universal-headers] project was started about 2 years
ago by now, but still quite far from completion. This project is important for
me, because I want to easily compile my software with different versions of
glibc using only the command-line switch. Zig already offers that for the
symbols exposed by glibc, but not for the headers. Currently zig offers headers
for only latest glibc versions, which [is][9485] [problematic][10713].

My take on universal-headers:
1. Compress all headers with `zstd --ultra -22`
2. Make the compressed tarball [seekable][seekable], so it can be extracted
   faster.
3. When user requests a specific glibc version, extract the headers for that
   particular version only and point the compiler to that directory.

Currently [`ziglang@universal-headers/headers`][all-headers], when compressed
naïvely, don't weigh that much:

    $ ls --block-size=M -1s headers.tar headers.tar.zst
    1121M headers.tar
      17M headers.tar.zst

Granted, they are incomplete (looks like only glibc 2.33-2.36 are present,
whereas we want at least [2.17][rhel-glibc]), but a reasonable start.

[universal-headers]: https://github.com/ziglang/universal-headers
[9485]: https://github.com/ziglang/zig/issues/9485
[10713]: https://github.com/ziglang/zig/issues/10713
[seekable]: https://github.com/facebook/zstd/blob/7806d803383b75b00868a5367154a18caf535a92/contrib/seekable_format/zstd_seekable_compression_format.md
[all-headers]: https://github.com/ziglang/universal-headers/tree/093de8cdcc2b7cb1d50342ef79812a864a8fde74/headers
[rhel-glibc]: https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/7.0_release_notes/sect-red_hat_enterprise_linux-7.0_release_notes-compiler_and_tools-glibc

Project 3: home and vacation
----------------------------

Quite some things have accumulated to improve and fix at home. That will get
some love and attention too.

Looking forward to all of the above!
