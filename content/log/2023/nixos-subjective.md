---
title: "Nixos Subjectively"
date: 2023-08-31T08:30:14+03:00
---

Previously I [bloggged]({{< ref "log/2023/end-of-summer.md" >}}) about the cool
things I did with NixOS. After publishing the post, my friend [Anton][alavrik]
promptly asked:

> Wondering what's your professional take on NixOS. Would you give it a short
> for a small-to-medium size server fleet provisioning? It felt rather involved
> and not very mature when I looked at it. Kind of a commitment, too.

Here is my response:

My journey to NixOS has been bumpy ride: it's been over a year since I looked
at first, and I still sometimes feel I did not escape the beginner level. The
learning curve is steep, and it is best to take it on gently or have a good
mentor nearby. I started by installing NixOS on my primary laptop, which was
a mistake. The annoyance of "I can do this in Debian in 5 seconds, and I am
an hour in without an end of sight in this thing" was very discouraging at
times.

I reinstalled my laptop back to Debian and took a few slow months to
provision 2 personal servers (the thing that's detailed in the blog). Taking
it slow has been fantastic experience. The folks in Matrix are very helpful
where documentation, especially high-level, is patchy. Now I feel comfortable
enough to retry NixOS on my laptop again.

Recently I realized that what I originally perceived as immaturity later
turned out lack of knowledge and/or lack of high-level documentation.
Technicals are good. Granted, I have found some bugs (though trivially
[fixed][nixos-prs]), but they mostly come from the power to configure it and
thus the huge surface area. Also, variety does not help: for example, there
are [10 deployment tools][deployment-tools] in the wiki ("nixops related"
counts too). It is hard to choose when I don't know what to expect, much less
know what's possible. It is also nontrivial to ask for a "high-level" advice:
a beginner will just tell their favorite system, not knowing the trade-offs
or alternatives. An expert will tell "depends on what you want to do". Moving
beyond such answer requires time and a beverage, which brings it's own
constraints. In this concrete case, I spent quite some time learning krops,
which later turned out to be a dead-end. Later moved to deploy-rs, which
turned out to be a good decision so far.

As far as recommendations go. For smaller companies, especially where
developers are also taking care of operations/deployments/infrastructure, I
can't recommend NixOS enough. For medium-large size companies it would
certainly bring a lot of value (I can already see how many things mine or my
sister-team at Uber had to re-implement which come out of the box in NixOS),
but, like with anything that has a different paradigm, requires a mentality
shift, which may be very hard organizationally.

There is at least one large-ish company I know that uses NixOS
([proof][canva]). I did not look, I found it by accident. I also know a few
folks in Tweag; their primary consulting stream is helping companies onboard
to Bazel and/or Nix. They won't tell who they are, but there are "quite a
few, of different sizes, flying under the radar".

[nixos-prs]: https://github.com/NixOS/nixpkgs/pulls?q=is%3Apr+author%3Amotiejus+is%3Aclosed
[deployment-tools]: https://nixos.wiki/wiki/Applications#Deployment
[canva]: https://opencollective.com/canvaofficial/expenses/115338
[alavrik]: https://github.com/alavrik
