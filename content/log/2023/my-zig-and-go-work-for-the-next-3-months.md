---
title: "My Zig and Go work for the next 3 months"
date: 2023-03-24T19:10:12+02:00
---

Now that Uber [uses "zig cc" to compile it's Go Monorepo][1], here is a sneak
preview of what I have planned for the [next 3 months]({{< ref
"log/2023/7-years-at-uber" >}}) on the Zig/Go side:

- Finish an "official" Uber's blog post about how Uber uses zig and why. The
  content will be similar to my [post a year ago]({{< ref
  "log/2022/how-uber-uses-zig" >}}), but with the milestones closed. Since the
  previous post we have enabled zig-cc as the default C/C++ compiler for CI,
  dev and production, our toolchain got open sourced and the company has
  committed to maintaining it. We still find bugs in Zig and Go and interaction
  between both though, so there is still work to be done.
- Play around with MacOS *target* support for the mentioned toolchain. I can't
  promise anything, but at least I want to evaluate the feasibility. This needs
  a bit of ground work on the toolchain side plus a lot of testing.
- Make all Go tests pass with `CC="zig cc"`. This is a prerequisite for the
  next goal.
- Propose and, if accepted, implement an official [CI node][2] that tests Go
  with "zig cc" as the C compiler for linux/amd64 and linux/arm64. When this is
  done, the Go team will be alerted of regressions that impact "zig cc" users
  during the change. At the moment, unfortunately, if Go has a regression that
  impacts "zig cc", the Go team learns about it only when someone (we?) attempt
  to upgrade Go to the latest release.
- Extension from the above: do the same for macos. This would have caught [this
  issue](https://github.com/golang/go/issues/58935) way before the release of
  Go 1.20; it will now be fixed only in Go 1.20.3.

If you are my (ex-)colleague that is/was involved, thank you very much. We have
such an amazing team that was able to pull this off.

[1]: https://twitter.com/mo_kelione/status/1633474830577160195
[2]: https://cs.opensource.google/go/x/build/+/master:dashboard/builders.go
