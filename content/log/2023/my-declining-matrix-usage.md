---
title: "My Declining Matrix usage"
date: 2023-08-31T09:00:19+03:00
---

This post explains my journey with [Matrix/Element][matrix]. Here is a short
timeline:

* 2020 November: read [A Matrix overview][lwn] on LWN and installed on my home
  server. Added Messenger, Hangouts and WhatsApp bridges. "Bridges" meant that
  I could read and write messages from FB Messenger, Google Hangouts and
  WhatsApp using a single application --- Element (the application for the
  Matrix network). After 2 evenings of installation hurdles I conveniently
  reduced the number of browser tabs and phone applications I use for
  messaging.
* 2021 February: convinced my brother to move to Element. Conversations between
  us (which are second-in-volume after my wife) were now hosted by me.
* 2021 April: rest of the family to moved to Matrix. Much-reduced usage of
  WhatsApp, Messenger and Hangouts. It felt great and liberating!
* 2023 July: switched my family to Signal.

First year of Matrix was promising. Matrix posted good news about adoption to
their blog. I followed them closely in conferences (you can see my name in one
of the screenshots [here!][fosdem]).

Then bugs started creeping in.

The first thing I noticed is that [Element from f-droid][f-droid] would
sometimes not deliver notifications. Reading about it on forums revealed that
applications on f-droid use a different notification mechanism than Google Play
apps do. Re-installed Google Play version and it worked fine. Not much to be
grumpy about, but it would be a nice touch to explain this in the landing page
of the Element f-droid version.

The second, and still ongoing, annoyance was *occasionally not being able to
see 1:1 messages from my family*, because it would fail to decrypt. [I am not
the only one][element-encrypt-bug]. I've been waiting for a fix since the very
beginning.

The third was a brittle audio/video setup. Calls to my family were hit-or-miss:
sometimes they would work well, sometimes audio would be one-way or would not
connect at all. I followed the [manual][turn] to the letter, but it still
couldn't make it work reliably. I was a "VoIP expert" at some point in my
career, so troubleshooting this was not completely alien. But I still lacked
knowledge and/or persistence to fix it "once and for all", which meant I just
gave up on audio/video. *It should be easier.*

Forward to mid-2023:
- I was not able to read a few important messages from my close family due to
  above-mentioned "failed to decrypt" bug.
- Opening some channels with long history would take minutes. [I have been
  warned][sqlite], so it's my fault. But there is a limit on how much complex I
  want my messaging stack to be.
- My Matrix server was down for a week due to an upstream [bug][bug-sqlite].
  Same problem --- using SQLite --- but same answer as above.
- [Libera.Chat bridge got shut down][libera-bridge], which was a big use-case
  for my Matrix use. As of writing it's been 27 days without an update, so it
  does not feel like Matrix folks are treating this shutdown like a real
  incident.
- Audio/video calling got even worse: it would be a surprise if a call
  connected successfully, and I lost motivation to debug it further. Should
  audio/video be the only problem, I would probably muster. But this just added
  up to the pile which I wanted to touch less and less.

At this point I started looking for a reliable alternative. By now I had added
Signal to communicate with some of my friends. I explained to my family what
Signal is and asked whether it would be OK to switch. Everyone agreed, we have
done it. Now they even have a wider bubble of people to talk in that platform,
audio/video calling works reliably, and I don't ever remember missing a
notification.

I still use Matrix to join channels of some open-source projects. The channels
are not encrypted, thus not hitting the "failed to decrypt" bug. I also wiped
the SQLite database (it was ~6 GB by the time), which made things very fast
again. I will leave my Matrix contact details and you can still message me on
Matrix. But honestly, due to all the reasons above, I prefer Signal by now,
even if it's not self-hosted.

[matrix]: https://matrix.org/
[lwn]: https://lwn.net/Articles/835880/
[fosdem]: https://matrix.org/blog/2021/02/15/how-we-hosted-fosdem-2021-on-matrix/
[f-droid]: https://f-droid.org/en/packages/im.vector.app/
[element-encrypt-bug]: https://github.com/vector-im/element-android/issues/1721
[turn]: https://matrix-org.github.io/synapse/v1.48/turn-howto.html
[libera-bridge]: https://matrix.org/blog/2023/08/libera-bridge-disabled/
[sqlite]: https://matrix-org.github.io/synapse/latest/setup/installation.html#using-postgresql
[bug-sqlite]: https://github.com/NixOS/nixpkgs/issues/239833
