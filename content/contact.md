---
title: "Contact"
description: "Motiejus Jakštys contact details"
---

- **Email**: [motiejus@jakstys.lt](mailto:motiejus@jakstys.lt)
- **Matrix**: [@motiejus:jakstys.lt](https://matrix.to/#/@motiejus:jakstys.lt)
- **Phone / Signal**: +37062951392
- **Libera.chat**: motiejus
- **PGP**: [6F133A0C1C2848D7](/gpg.txt)

Unless otherwise stated, &copy;Motiejus Jakštys. [CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/).
